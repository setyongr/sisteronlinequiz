import json
import socket
import curses
import threading
import time


HOST = "127.0.0.1"
PORT = 1235
answer = []

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Init curses
screen = curses.initscr()
curses.noecho()
curses.curs_set(0)
screen.keypad(1)

timer_on = True

def input():
    # Setyo
    curses.echo()
    s = screen.getstr().decode()
    curses.noecho()
    return s

def print(*args, end="\n"):
    # Setyo
    s = ""
    for arg in args:
        s += str(arg)
    
    s += end
    screen.addstr(s)

def countdown(screen, t):
    # Setyo
    global timer_on
    while t and timer_on:
        mins, secs = divmod(t, 60)
        timeformat = '{:02d}:{:02d}'.format(mins, secs)
        screen.addstr(0, 50, timeformat)
        screen.refresh()
        time.sleep(1)
        t -= 1
    
    if timer_on:
        screen.addstr(0, 50, "WAKTU HABIS!!!")
        screen.refresh()
        timer_on = False

def send_answer():
    # Cindy kirim jawaban
    s.sendall(json.dumps(answer).encode())

def set_name():
    #arif set nama
    input_nama = True
    while input_nama:
        print("Masukkan nama: ", end = "")
        name = input()
        s.sendall(name.encode())
        hasil = s.recv(1024).decode()
        if hasil == "oke":
            input_nama = False
        else:
            print("Nama sudah dipakai")
    s.send("end".encode())

def get_question():
    # Setyo: Tampilkan pertanyaan
    global timer_on
    quiz_time = int(s.recv(1027).decode())
    data = s.recv(1024)
    question = json.loads(data)
    num = 1
    
    timer_on = True
    clock = threading.Thread(target=countdown, args=(screen,quiz_time))
    clock.daemon = True
    clock.start()
    for i, q in enumerate(question):
        need_answer = True
        ans = ""
        while need_answer:
            screen.clear()
            print("Pertanyaan ", num)
            print(q['question'])
            for i, a in enumerate(q['answer']):
                print(i + 1, " = ", a)

            print("Jawab dengan menekan nomor jawaban ", end="")
            e = screen.getch()
            ans = str(chr(e))
            posible_answer = range(len(q['answer']) + 1)
            if ans in map(lambda x: str(x), posible_answer):
                need_answer = False

        answer.append(ans)
        num += 1

        if not timer_on:
            break

    timer_on = False
    screen.clear()
    print("Mengirim jawaban")
    send_answer()
    # Cindy tampilkan nilai
    screen.clear()
    nilai = s.recv(1024).decode()
    print("Nilai Anda Adalah : ", nilai)

    # COTS Cindy
    print("Result...")
    result = json.loads(s.recv(1024).decode())
    print("Jawaban yang salah")

    for l in result:
        print(l)
        num += 1

    # Ija
    print("Menunggu Leaderboard...")
    screen.refresh()
    leaderbord = json.loads(s.recv(1024).decode())
    print("Leader board")
    num = 1

    for l in leaderbord:
        print(num, " ", l[0], " = ", l[1])
        num += 1

    waktu = json.loads(s.recv(1024).decode())
    print("Waktu")
    num = 1

    for l in waktu:
        print(num, " ", l[0], " = ", l[1], " menit")
        num += 1
    

s.connect((HOST, PORT))
set_name()
get_question()
screen.addstr("Tekan tombol untuk mengakhiri")
event = screen.getch()
curses.endwin()