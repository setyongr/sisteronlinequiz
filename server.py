import json
import socket
from _thread import *
from datetime import datetime

host = "127.0.0.1"
port = 1235
client = {}
quiz_time_second = 30

# Cindy
quiz = [
    {
        "question": "Manakah dibawah ini yang bukan bahasa pemrograman?",
        "answer": [
            "Python", "Ruby", "Bosque", "Jolang"
        ]
    },
    {
        "question": "Manakah yang bukan Kelompok Keahlian dalam Teknik Elektro?",
        "answer": [
            "RIE", "PSI", "WW", "SE"
        ]
    },
    {
        "question": "Contoh deep learning",
        "answer": [
            "RNN", "ANN", "MLP", "SOM"
        ]
    },
    {
        "question": "Berikut TUPRO Machine Learning kecuali ",
        "answer": [
            "Naive Bayes", "Random Forest", "Ensemble", "Q-Learning"
        ]
    }
]

truth = [4, 3, 1, 2]

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port))

waktu_mulai = datetime.now()
def set_name(conn, addr):
    #arif set nama
    name = ""
    minta_nama = True
    while minta_nama:
        name = conn.recv(1024).decode()

        # Cek apakah ada nama yang sama
        sama = False
        for c in client:
            if name == client[c].get("name"):
                sama = True
        
        if sama:
            conn.send("sama".encode())
        else:
            conn.send("oke".encode())
            minta_nama = False
            
    conn.recv(1024)
    client[addr]["name"] = name

# COTS Cindy
def wrong_answer(conn, addr, answer) :
    wrong = []
    for i in range (len(answer)) :
        if truth[i] != int(answer[i]) :
            wrong.append('Jawaban untuk nomor ke ' + str(i+1) + ' adalah = ' + answer[i])
    
    conn.send(json.dumps(wrong).encode()) 

def calculate_result(conn, addr, answer):
    # Cindy Hitung nilai
    j = 0
    for i in range (len(answer)) :
        if truth[i] == int(answer[i]) : j += 1
        
    nilai = j / len(truth) * 100
    
    client[addr]["nilai"] = nilai
    
    conn.sendall(str(nilai).encode())
        
def client_handler(conn, addr):
    # Handle setiap client
    set_name(conn, addr)
    
    # Setyo, Kirim Pertanyaan
    conn.sendall(str(quiz_time_second).encode())
    conn.sendall(json.dumps(quiz).encode())
    
    # Cindy ambil dan hitung jawaban
    answer = conn.recv(1024).decode()
    calculate_result(conn, addr, json.loads(answer))

    # Tambahan ku
    wrong_answer(conn, addr, json.loads(answer))

    client[addr]['waktu_kirim'] = datetime.now()

def start_event_loop():
    # Jalankan perulangan tak hingga dan terima koneksi
    try:
        while True:
            c, addr = s.accept()
            client[addr] = {
                "conn": c
            }
            start_new_thread(client_handler, (c, addr))
    except KeyboardInterrupt:
        # Tunggu sampe user tekan CTRL+C
        # Buat list leaderboard
        leaderboard = []
        waktu = []

        for k in client:
            data = [client[k]['name'], client[k]['nilai']]
            leaderboard.append(data)

            selisih = client[k]['waktu_kirim'] - waktu_mulai
            menit = selisih.seconds / 60

            data_waktu = [client[k]['name'], menit]
            waktu.append(data_waktu)
        
        # Sorting descending berdasarkan nilai
        leaderboard = sorted(leaderboard, key = lambda x: x[1], reverse = True) 
        waktu = sorted(waktu, key = lambda x: x[1])

        #Tampilkan Leaderboard
        print("Leaderboard:")
        num = 1
        for l in leaderboard:
            print(num, " ", l[0], " = ", l[1])
            num += 1
        
        # Ija
        for k in client:
            client[k]["conn"].send(json.dumps(leaderboard).encode()) 

        #Tampilkan waktu
        print("Waktu:")
        num = 1
        for l in waktu:
            print(num, " ", l[0], " = ", l[1], "menit")
            num += 1
        
        # Ija
        for k in client:
            client[k]["conn"].send(json.dumps(waktu).encode()) 
            
        s.close()

s.listen(5)
start_event_loop()
