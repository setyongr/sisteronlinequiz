# Tugas-Besar_Online_Quiz

Nama: 
* Setyo Nugroho
* Cindy Alifia Putri
* Vrieza Rizqiya 
* Arif Rahman

Kelas: IF-40-12

## Requirement

*  Python 3.6

## How to run

### Server

Enter command `python server.py`

Untuk mengkahiri dan menampilkan leaderboard tekan `CTRL+C`

### Client

Enter command `python client.py`

## Fitur

- Nama peserta
- Nama peserta tidak boleh sama
- Waktu quis
- Memperlihatkan jawaban yang salah
- Leaderboard berdasarkan nilai
- Leaderboard berdasarkan waktu

